import yaml
import os
import re

dotyaml = re.compile(r'(\w*)\.yaml')
ls = os.listdir('locales')
_locales = [dotyaml.search(i) for i in ls]
_locales = [i.group(1) for i  in _locales if i]

class LangMap(dict):
    def __init__(self, locales: list = [], default = 'en'):
        if not list: return
        self.default = default
        if not default in locales:
            raise ValueError("Defualt language not defined")
        
        with open(f'locales/{default}.yaml') as df:
            self[default] = LangMap.__new__(LangMap)
            self[default].update(yaml.safe_load(df))

        for lang in locales:
            with open(f'locales/{lang}.yaml') as f:
                self[lang] = LangMap.__new__(LangMap)
                self[lang].update(self[default])
                self[lang].update(yaml.safe_load(f))

    def Get(self, *args):
        copy = self
        for k in args:
            copy=copy[k]
        return copy

    def __call__(self, lang:str = 'en'):
        if lang in self:
            return self[lang]
        else:
            return self[self.default]
        
LOCALES = LangMap(_locales)

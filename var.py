from aiogram import Bot, Dispatcher

from locales import LOCALES
import config

bot = Bot(config.BOT_API_TOKEN)
dp = Dispatcher(bot)

from dotenv import load_dotenv
from os import environ as env

load_dotenv()
PORT = int(env.get('PORT'))
DATABASE_URL = env.get('DATABASE_URL')
#if '?' in DATABASE_URL:
#    DATABASE_URL = DATABASE_URL.split('?')[0]
BOT_API_TOKEN = env.get('BOT_API_TOKEN')
WEBHOOK = env.get('WEBHOOK')+ '/'

WEBHOOK_PATH = WEBHOOK+BOT_API_TOKEN

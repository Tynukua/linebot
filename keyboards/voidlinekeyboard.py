
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


class VoidLineKeyboard(InlineKeyboardMarkup):
    def __init__(self, title, lang=None):
        text = 'Start'
        if lang:
            try:
                text = lang.Get('keyboards', 'VoidLineKeyboard')
            except Exception: pass
        super().__init__(inline_keyboard = [
            [
                InlineKeyboardButton(text,
                    callback_data=f'start {title}'),

                ],

            ])

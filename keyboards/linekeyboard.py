from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

class LineKeyboard(InlineKeyboardMarkup):
    def __init__(self, line_id, lang=None):
        add_text = 'Add'
        exit_text = 'Exit'
        skip_text = 'Skip'
        end_text = 'To end'
        if lang:
            try:
                lang = lang.Get('keyboards', 'LineKeyboard')
                add_text = lang['add']
                exit_text = lang['exit']
                skip_text = lang['skip']
                end_text = lang['to_end']
            except Exception: pass
        super().__init__(inline_keyboard = [
            [
                InlineKeyboardButton(add_text,
                    callback_data=f'line-add {line_id}'),
                InlineKeyboardButton(exit_text,
                    callback_data=f'line-exit {line_id}'),

                ],
            [
                InlineKeyboardButton(skip_text, 
                    callback_data=f'line-skip {line_id}'),
                ],
            [
                InlineKeyboardButton(end_text,
                    callback_data=f'line-end {line_id}'),
                ],

            ])
        

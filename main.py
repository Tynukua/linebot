import re
import asyncio
import logging

from aiogram import Bot, Dispatcher, executor, types

import database as db
from database.models import Line, TelegramUser
import config
import handlers
from var import bot, dp

logging.basicConfig(level=logging.INFO)

if __name__ =="__main__":
    import sys
    webhook = 'webhook' in sys.argv

    async def on_startup(dp):
        logging.info("Database URL:"+config.DATABASE_URL)
        await db.init()
        asyncio.create_task(db.line_exec.run())
        await db.models.Line.delete_old(dp.bot)

        if(webhook):
            await dp.bot.set_webhook(config.WEBHOOK_PATH)

    async def on_shutdown(dp):
        await db.Tortoise.close_connections()

    if webhook:
        executor.start_webhook (dp, 
            webhook_path = f'/{config.BOT_API_TOKEN}',
            on_shutdown = on_shutdown,
            on_startup  = on_startup,port = config.PORT)
    else:
        executor.start_polling(dp,
            on_shutdown = on_shutdown,
            on_startup  = on_startup,
            skip_updates=True)

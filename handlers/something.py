import re
import asyncio

import aiogram
from aiogram.dispatcher.filters.builtin import Command
from .util import update_user, is_reply_line
from database.models import TelegramUser, Line
from database import line_exec
from var import dp, bot, LOCALES

bot: aiogram.Bot = dp.bot

@dp.message_handler( Command(['setname']))
async def _setname(msg: aiogram.types.Message,
            command: Command.CommandObj    ) :
    lang = LOCALES(msg.from_user.language_code)
    command.args: str = msg.get_args()
    user, _ = await TelegramUser.get_or_create(user_id = msg.from_user.id)
    await user.set_name(command.args.replace('\n', ' ' ))
    if not command.args:
        await user.update_name(msg.from_user.full_name)
    if not command.args:
        name = msg.from_user.full_name
        text = lang.Get('set_name', 'null_name')
    else:
        name = command.args
        text = lang.Get('set_name', 'new_name')
    comment = lang.Get('set_name', 'comment')
    await msg.answer(text.format(name = name)+'\n'+f'<i>{comment}</i>',
            parse_mode='HTML')


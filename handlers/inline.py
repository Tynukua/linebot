import re

from aiogram.types import InlineQuery, InlineQueryResultArticle, InputTextMessageContent, CallbackQuery
import var
from var import bot, LOCALES, dp
from datetime import datetime
from keyboards.voidlinekeyboard import VoidLineKeyboard
from database.models import Line

@var.dp.inline_handler()
async def _send_void_line(inline_q: InlineQuery):
    lang = var.LOCALES(inline_q.from_user.language_code)
    title = inline_q.query
    if not title: title = lang.Get('title')
    if len(title)>120:
        title = title[:120]
    item = InlineQueryResultArticle(id=str(hash(title)),
           title=title,
           reply_markup=VoidLineKeyboard(title, lang),
           input_message_content=InputTextMessageContent(title)
        )

    await inline_q.answer([item],cache_time=2)

start= re.compile('start (.+)')
@var.dp.callback_query_handler(regexp = start)
async def _start_line(call: CallbackQuery):
    title = start.search(call.data).group(1)
    line = await Line.create(title = title, 
            jsonv={},
            message_id = 0,
            chat_id = 0,
            inline_message_id= call.inline_message_id,
            language_code = call.from_user.language_code,
            )
    await line.save()
    await line.update_message(bot)



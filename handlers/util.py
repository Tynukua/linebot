from database.models import TelegramUser, Line
from aiogram.types import Message
from aiogram.types import CallbackQuery

def update_user(func):
    async def new_func(msg: Message):
        user: TelegramUser =  await TelegramUser.get_or_create(user_id = msg.from_user.id)
        await user[0].update_name(msg.from_user.full_name)
        return await func(msg)
    return new_func

async def is_reply_line(msg: Message):
    msg = msg.reply_to_message
    if msg:
        return await Line.filter(
            chat_id = msg.chat.id, 
            message_id = msg.message_id).exists()

def _is_line_exists(regexp):
    def _is_line_exists(handler):
        async def new_handler(call: CallbackQuery):
            line_id = int(regexp.search(call.data).group(1))
            line = await Line.get_or_none(id = line_id)
            if line:
                await handler(call, line)

            else: 
                await call.answer('pufff')
        return new_handler
    return _is_line_exists

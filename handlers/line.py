import re
import asyncio
from datetime import datetime

import aiogram
from .util import update_user, is_reply_line
from database.models import TelegramUser, Line
from database import line_exec
from database.exceptions import LocaledException

from var import bot, LOCALES, dp


line_start_cmd =  re.compile(r'\/l[@\d\w]* ?(.*)')

@dp.message_handler(regexp = line_start_cmd)
@update_user
async def _start_line(msg:aiogram.types.Message):
    lang = LOCALES(msg.from_user.language_code)
    match = line_start_cmd.search(msg.text)
    title = match.group(1)
    if not title: title = str(lang.Get('title')) 
    langc = msg.from_user.language_code
    l = await Line.create(chat_id = msg.chat.id, jsonv={}, title = title, 
            language_code= langc if langc else 'en')
    await l.update_message(dp.bot)


line_add = re.compile(r'line-add (\d+)')
    
async def add_line_atom(line:Line, call, bot, number = None):
    lang = LOCALES(call.from_user.language_code)
    try: 
        await line.add(call.from_user.id,number)
        await line.update_message(bot)
    except LocaledException as ex:
        try:
            await call.answer(ex.text(lang))
        except Exception as ex:
            print (ex)

@dp.callback_query_handler(regexp = line_add)
@update_user
async def _add_line(call: aiogram.types.CallbackQuery):
    match = line_add.search(call.data)
    line_id = int(match.group(1))
    line: Line = await Line.get(id = line_id)
    line_exec.add_line_editor(line.id, add_line_atom(line,call,dp.bot))


numbermask = re.compile(r"(\d{1,4})")

@dp.message_handler(is_reply_line, regexp=numbermask)
async def _number_add(msg:aiogram.types.Message):
    num = int(numbermask.search(msg.text).group(1))
    if not num: 
        return
    line: Line = await Line.filter(
            chat_id = msg.reply_to_message.chat.id,
            message_id = msg.reply_to_message.message_id).first()
    line_exec.add_line_editor(line.id, add_line_atom(line,msg,dp.bot, num))

line_skip = re.compile(r'line-skip (\d+)')
async def skip_line_atom(line, call, bot):
    lang = LOCALES(call.from_user.language_code)
    try: 
        await line.skip(call.from_user.id)
        await line.update_message(bot)
    except LocaledException as ex:
        await call.answer(ex.text(lang))

@dp.callback_query_handler(regexp = line_skip)
@update_user
async def _skip_line(call: aiogram.types.CallbackQuery):
    match = line_skip.search(call.data)
    line_id = int(match.group(1))
    line: Line = await Line.get(id = line_id)
    line_exec.add_line_editor(line.id, skip_line_atom(line,call,dp.bot))


line_end = re.compile(r'line-end (\d+)')
async def end_line_atom(line, call, bot):
    lang = LOCALES(call.from_user.language_code)
    try: 
        await line.to_end(call.from_user.id)
        await line.update_message(bot)
    except LocaledException as ex:
        print(ex)
        await call.answer(ex.text(lang))

@dp.callback_query_handler(regexp = line_end)
@update_user
async def _end_line(call: aiogram.types.CallbackQuery):
    match = line_end.search(call.data)
    line_id = int(match.group(1))
    line: Line = await Line.get(id = line_id)
    line_exec.add_line_editor(line.id, end_line_atom(line,call,dp.bot))

line_exit = re.compile(r'line-exit (\d+)')
async def exit_line_atom(line, call, bot):
    lang = LOCALES(call.from_user.language_code)
    try: 
        await line.exit(call.from_user.id)
        await line.update_message(bot)
    except LocaledException as ex:
        await call.answer(ex.text(lang))

@dp.callback_query_handler(regexp = line_exit)
@update_user
async def _exit_line(call: aiogram.types.CallbackQuery):
    match = line_exit.search(call.data)
    line_id = int(match.group(1))
    line: Line = await Line.get(id = line_id)
    line_exec.add_line_editor(line.id, exit_line_atom(line,call,dp.bot))

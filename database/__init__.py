import sys

from tortoise import Tortoise

import config
import database.models
from database.lineexec import LineExecutor

line_exec = LineExecutor()

async def init():
    await Tortoise.init(
        db_url=config.DATABASE_URL,
        modules={'models': ['database.models']} )
    await Tortoise.generate_schemas(safe= not "testdb" in sys.argv)


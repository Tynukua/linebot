from tortoise.models import Model
from tortoise import fields
import aiogram
from database import exceptions
from keyboards.linekeyboard import LineKeyboard
from json import loads
from datetime import datetime
import asyncio
from locales import LOCALES
class TelegramUser(Model):
    user_id = fields.IntField(True)
    name    = fields.TextField(default= '')
    _is_name_setted = fields.BooleanField(default=False)

    async def update_name(self, name: str):
        if not self._is_name_setted:
            await TelegramUser.filter(
                user_id = self.user_id).update(name = name)
    @classmethod
    async def get_name(cls, id: int):
        return (await cls.filter(user_id = id).first()).name

    async def set_name(self, name: str = None):
        if name:
            await self.filter(user_id = self.user_id).update(
                _is_name_setted = True,
                name = name
                    )
        else:
            await self.filter(user_id = self.user_id).update(
                _is_name_setted = False
                    )


def jsonloads(jsontext:str):
    d = loads(jsontext)
    ret = dict()
    for i in d.keys():
        if i.isdigit():
            ret[int(i)] = d[i]
    return ret


class Line(Model):
    id      = fields.IntField(True)
    jsonv   = fields.JSONField(null=1, decoder=jsonloads)
    title   = fields.TextField()
    chat_id    = fields.BigIntField()
    message_id = fields.IntField(default=-1)
    inline_message_id = fields.TextField(default='')
    last_update = fields.DatetimeField(auto_now=True,tzinfo = None)
    language_code = fields.CharField(2,default='en')


    async def str(self):
        ret = f'{self.title}\n'
        for i in sorted(self.jsonv.keys()):
            ret += f'{i}. '    
            ret += f'<a href="tg://user?id={self.jsonv[i]}">{await TelegramUser.get_name(self.jsonv[i])}</a>\n'
        return ret 

    async def refresh(self):
        data = await type(self).filter(id = self.id).first()
        self.jsonv = data.jsonv

    async def save_json(self):
        await self.save()
    # await self.filter(id = self.id).update(
    #        jsonv = self.jsonv)

    async def add(self, user_id: int, number:str = None, coro_callback = None):
        await self.refresh()
        if user_id in self.jsonv.values():
            raise exceptions.LineUserExists(user_id)
        if number is None: #find free number in line
            if not self.jsonv:
                number = 1
            else:
                for i in range(max(self.jsonv.keys())+1):
                    if not i+1 in self.jsonv.keys():
                        number = i+1
                        break

        if not number in self.jsonv:
            self.jsonv[number] = user_id
            await self.save_json()
        else:
            raise exceptions.LinePlaceTaken(number)
        if coro_callback:
            await coro_callback

    def get_number(self, user_id):
        key_list = list(self.jsonv.keys()) 
        val_list = list(self.jsonv.values())
        if not user_id in val_list:
            raise exceptions.LineUserNotExists(user_id)
        return key_list[val_list.index(user_id)]

    async def skip(self, user_id: int, coro_callback = None):
        await self.refresh()
        if not user_id in self.jsonv.values():
            raise exceptions.LineUserNotExists(user_id)

        number = self.get_number(user_id) 
        new_number = int(number) + 1

        if new_number in self.jsonv:
            self.jsonv[number],self.jsonv[new_number] = (
                    self.jsonv[new_number], self.jsonv[number])
        else:
            self.jsonv[new_number] = self.jsonv[number]
            del self.jsonv[number]
        await self.save_json()
        if coro_callback:
            await coro_callback

    async def to_end(self, user_id, coro_callback = None): 
        await self.refresh()
        number = self.get_number(user_id)
        maxv = max(self.jsonv.keys())
        if maxv != number:
            self.jsonv[maxv+1] = self.jsonv[number]
            del self.jsonv[number]
        await self.save_json()
        if coro_callback:
            await coro_callback


    async def update_message(self,bot: aiogram.Bot):
        reciever = {'inline_message_id': self.inline_message_id} if self.inline_message_id else {'chat_id': self.chat_id, 'message_id': self.message_id}
        lang = LOCALES(self.language_code)
        await self.refresh()
        text = await self.str()
        try:
            await bot.edit_message_text(
                text,
                **reciever,
                reply_markup=LineKeyboard(self.id, lang),
                parse_mode='HTML' )
        except aiogram.exceptions.MessageToEditNotFound:
            msg = await bot.send_message(self.chat_id, text,
                reply_markup=LineKeyboard(self.id, lang),
                parse_mode='HTML')
            await self.filter(id = self.id).update(
                    message_id = msg.message_id)
        except aiogram.utils.exceptions.MessageNotModified:
            pass
        except Exception as ex: 
            print(ex, type(ex))
    
    async def exit(self, user_id):
        await self.refresh()
        try:
            num = self.get_number(user_id)
        except ValueError:
            raise exceptions.LineUserNotExists(user_id)
        del self.jsonv[num]
        await self.save_json()

    @classmethod
    async def delete_old(self, bot: aiogram.Bot):
        async for line in Line.all():
            delta = datetime.utcnow() - line.last_update.replace(tzinfo =None)
            if delta.days >=2:
                print(delta)
                await line.delete()


            

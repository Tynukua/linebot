class LocaledException(BaseException):
    def text(self, lang): pass

class LinePlaceTaken(ValueError, LocaledException):
    def __init__(self, n):
        self.taken_place = n

    def text(self, lang):
        return lang.Get('exceptions', 'LinePlaceTaken')


class LineUserExists(ValueError, LocaledException):
    def __init__(self, _id):
        self.user_id = _id

    def text(self, lang):
        return lang.Get('exceptions', 'LineUserExists')

class  LineUserNotExists(ValueError, LocaledException):
    def __init__(self, _id):
        self.user_id = _id

    def text(self, lang):
        return lang.Get('exceptions', 'LineUserNotExists')

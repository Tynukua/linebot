import asyncio
import logging
from .models import Line

async def run_item(item):
    item[0] = 'running'
    try:
        await item[1]
    except Exception as ex:
        logging.debug(ex)
    item[0] = 'end'
    

class LineExecutor(dict):
    def add_line_editor(self,
            line_id: int, 
            coroutine):
        if not line_id in self:
            self[line_id] = []
        self[line_id].append(['wait', coroutine])

    async def run(self):
        while 1:
            if not self:
                await asyncio.sleep(1)
                continue
            for line_id in list(self.keys()):
                await asyncio.sleep(0.5)
                if not self[line_id]:
                    del self[line_id]
                    continue
                if(self[line_id][0][0]=='wait'):
                    asyncio.create_task(run_item(self[line_id][0]))
                elif self[line_id][0][0]=='end':
                    self[line_id].pop(0)
                    
